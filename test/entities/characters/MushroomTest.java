package entities.characters;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Luca on 16/03/2017.
 */
public class MushroomTest {

    private static final int DEFAULT_MUSHROOM_X = 300;
    private static final int DEFAULT_MUSHROOM_Y = 245;
    private static final int DEFAULT_TURTLE_X = 350;
    private static final int DEFAULT_TURTLE_Y = 245;

    private Enemy mushroom;

    @Before
    public void initialization(){
        this.mushroom = new EnemyFactoryImpl().createMushroom(DEFAULT_MUSHROOM_X, DEFAULT_MUSHROOM_Y);
    }

    @Test
    public void moveToRight() throws Exception {
        assertEquals(this.mushroom.getX(), DEFAULT_MUSHROOM_X);
        this.mushroom.setToRight(true);
        Thread.sleep(1000);
        assertTrue(this.mushroom.getX() > DEFAULT_MUSHROOM_X);
    }

    @Test
    public void moveToLeft() throws Exception {
        assertEquals(this.mushroom.getX(), DEFAULT_MUSHROOM_X);
        this.mushroom.setToRight(false);
        Thread.sleep(1000);
        assertTrue(this.mushroom.getX() < DEFAULT_MUSHROOM_X);
    }

    @Test
    public void turnBackAfterTouchingAnotherEnemy() throws Exception {
        Enemy turtle = new EnemyFactoryImpl().createTurtle(DEFAULT_TURTLE_X, DEFAULT_TURTLE_Y);
        this.mushroom.setToRight(true);
        turtle.setToRight(false);
        int timeout = 0;
        while(timeout < 3000){
            this.mushroom.contact(turtle);
            turtle.contact(this.mushroom);
            Thread.sleep(1);
            timeout++;
        }
        assertTrue(this.mushroom.getX() < turtle.getX());
    }

}
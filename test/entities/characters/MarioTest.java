package entities.characters;

import entities.objects.Collectible;
import entities.objects.CollectibleFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Luca on 16/03/2017.
 */
public class MarioTest {

    private static final int DEFAULT_MARIO_X = 300;
    private static final int DEFAULT_MARIO_Y = 245;
    private static final int DEFAULT_STAR_X = 300;
    private static final int DEFAULT_STAR_Y = 245;
    private static final int DEFAULT_ENEMY_X = 260;
    private static final int DEFAULT_ENEMY_Y = 245;

    private Mario mario;

    @Before
    public void initialization(){
        this.mario = new Mario(DEFAULT_MARIO_X, DEFAULT_MARIO_Y);
    }

    @Test
    public void marioMustBeKilledTouchingAnEnemy() throws Exception{
        Enemy enemy = new EnemyFactoryImpl().createMushroom(DEFAULT_ENEMY_X, DEFAULT_ENEMY_Y);
        assertTrue(this.mario.isAlive());
        assertTrue(enemy.isAlive());
        int timeout = 0;
        while(timeout < 3000){
            this.mario.contact(enemy);
            Thread.sleep(1);
            timeout++;
        }
        assertFalse(this.mario.isAlive());
        assertTrue(enemy.isAlive());
    }

    @Test
    public void marioBecomesPowerAfterCollectedTheStar() throws Exception {
        Collectible star = new CollectibleFactoryImpl().createStar(DEFAULT_STAR_X, DEFAULT_STAR_Y);
        assertFalse(this.mario.isPower());
        this.mario.contactCollectible(star);
        assertTrue(this.mario.isPower());
    }

    @Test
    public void marioStopsBeingPowerAfter10Seconds() throws Exception {
        marioBecomesPowerAfterCollectedTheStar();
        int timeout = 0;
        while(timeout < 10){
            assertTrue(this.mario.isPower());
            Thread.sleep(1000);
            timeout++;
        }
        assertFalse(this.mario.isPower());
    }

    @Test
    public void marioMustKillEnemyWhenPower() throws Exception{
        Enemy enemy = new EnemyFactoryImpl().createMushroom(DEFAULT_ENEMY_X, DEFAULT_ENEMY_Y );
        assertTrue(enemy.isAlive());
        marioBecomesPowerAfterCollectedTheStar();
        int timeout = 0;
        while(timeout < 3000){
            this.mario.contact(enemy);
            Thread.sleep(1);
            timeout++;
        }
        assertFalse(enemy.isAlive());
        assertTrue(this.mario.isAlive());
    }

}
package game;

import entities.Entity;
import entities.objects.*;
import entities.characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import entities.objects.Object;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private Mario mario;
    private List<Enemy> enemies;

    private List<Object> objects;

    private List<Collectible> collectibles;

    private EnemyFactory enemyFactory;
    private CollectibleFactory collectibleFactory;
    private ObjectFactory objectFactory;

    private Image imgFlag;
    private Image imgCastle;

    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        initializeGameEntities();

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());

    }

    public Mario getMario(){
        return mario;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = (getMario().isPower()? mov * 2 : mov);
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.getxPos() >= 0 && this.getxPos() <= 4600) {
            this.setxPos(this.getxPos() + this.getMov());

            this.background1PosX = this.background1PosX - this.getMov();
            this.background2PosX = this.background2PosX - this.getMov();
        }

        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (Object object : objects) {
            if (this.mario.isEntityNearby(object)) {
                this.mario.contactObject(object);
            }
            for(Enemy enemy : enemies) {
                if (enemy.isEntityNearby(object)) {
                    enemy.contact(object);
                }
            }
        }

        for (int i = 0; i < collectibles.size(); i++) {
            if (this.mario.contactCollectible(this.collectibles.get(i))) {
                if(collectibles.get(i) instanceof Piece) {
                    Audio.playSound(Res.AUDIO_MONEY);
                }
                this.collectibles.remove(i);
            }
        }

        for (Enemy firstEnemy : enemies){
            for (Enemy secondEnemy : enemies){
                if(firstEnemy != secondEnemy && firstEnemy.isEntityNearby(secondEnemy)){
                    firstEnemy.contact(secondEnemy);
                }
            }
            if(this.getMario().isEntityNearby(firstEnemy)){
                this.getMario().contact(firstEnemy);
            }
        }

        this.updateBackgroundOnMovement();
        if (this.getxPos() >= 0 && this.getxPos() <= 4600) {
            for (Object object : objects) {
                object.moveOnSceneShift();
            }

            for (Collectible collectible : collectibles) {
                collectible.moveOnSceneShift();
            }

            for (Entity enemy: enemies){
                enemy.moveOnSceneShift();
            }
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (Object object : objects) {
            g2.drawImage(object.getImageObject(), object.getX(),
                    object.getY(), null);
        }

        for (Collectible collectible : collectibles) {
            g2.drawImage(collectible.getImageObject(), collectible.getX(),
                    collectible.getY(), null);

        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping()) {
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        }else {
            g2.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO), this.mario.getX(), this.mario.getY(), null);
        }

        for(Enemy enemy : enemies) {
            if (enemy.isAlive()){
                g2.drawImage(enemy.walk(), enemy.getX(), enemy.getY(), null);
            }else {
                g2.drawImage(enemy.deadImage(), enemy.getX(), enemy.getY() + enemy.deadOffsetY(), null);
            }
        }
    }



    private void initializeGameEntities(){
        initializeMario();
        initializeEnemies();
        initializeObjects();
    }

    private void initializeMario(){
        mario = new Mario(300, 245);
    }

    private void initializeEnemies(){
        enemies = new ArrayList<>();
        enemyFactory = new EnemyFactoryImpl();
        initializeMushrooms();
        initializeTurtles();
    }

    private void initializeMushrooms(){
        enemies.add(enemyFactory.createMushroom(800, 263));
    }

    private void initializeTurtles(){
        enemies.add(enemyFactory.createTurtle(950, 243));
    }

    private void initializeObjects(){

        initializeStaticObjects();
        initializeCollectibles();

    }

    private void initializeStaticObjects(){
        objects = new ArrayList<>();
        objectFactory = new ObjectFactoryImpl();
        initializeTunnels();
        initializeBlocks();
    }

    private void initializeTunnels(){
        objects.add(objectFactory.createTunnel(600, 230));
        objects.add(objectFactory.createTunnel(1000, 230));
        objects.add(objectFactory.createTunnel(1600, 230));
        objects.add(objectFactory.createTunnel(1900, 230));
        objects.add(objectFactory.createTunnel(2500, 230));
        objects.add(objectFactory.createTunnel(3000, 230));
        objects.add(objectFactory.createTunnel(3800, 230));
        objects.add(objectFactory.createTunnel(4500, 230));
    }

    private void initializeBlocks(){
        objects.add(objectFactory.createBlock(400, 180));
        objects.add(objectFactory.createBlock(1200, 180));
        objects.add(objectFactory.createBlock(1270, 170));
        objects.add(objectFactory.createBlock(1340, 160));
        objects.add(objectFactory.createBlock(2000, 180));
        objects.add(objectFactory.createBlock(2600, 160));
        objects.add(objectFactory.createBlock(2650, 180));
        objects.add(objectFactory.createBlock(3500, 160));
        objects.add(objectFactory.createBlock(3550, 140));
        objects.add(objectFactory.createBlock(4000, 170));
        objects.add(objectFactory.createBlock(4200, 200));
        objects.add(objectFactory.createBlock(4300, 210));
    }

    private void initializeCollectibles(){
        collectibles = new ArrayList<>();
        collectibleFactory = new CollectibleFactoryImpl();
        initializeStars();
        initializePieces();
    }

    private void initializeStars(){
        collectibles.add(collectibleFactory.createStar(400, 140));
    }

    private void initializePieces(){
        collectibles.add(collectibleFactory.createPiece(1202, 140));
        collectibles.add(collectibleFactory.createPiece(1272, 95));
        collectibles.add(collectibleFactory.createPiece(1342, 40));
        collectibles.add(collectibleFactory.createPiece(1650, 145));
        collectibles.add(collectibleFactory.createPiece(2650, 145));
        collectibles.add(collectibleFactory.createPiece(3000, 135));
        collectibles.add(collectibleFactory.createPiece(3400, 125));
        collectibles.add(collectibleFactory.createPiece(4200, 145));
        collectibles.add(collectibleFactory.createPiece(4600, 40));
    }
}

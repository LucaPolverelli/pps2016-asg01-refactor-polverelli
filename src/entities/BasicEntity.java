package entities;

import game.Main;

/**
 * Created by Luca on 12/03/2017.
 */
public class BasicEntity implements Entity {

    private int width, height;
    private int x, y;

    public BasicEntity(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void moveOnSceneShift() {
        if (Main.scene.getxPos() >= 0) {
            this.setX(this.getX() - Main.scene.getMov());
        }
    }

}

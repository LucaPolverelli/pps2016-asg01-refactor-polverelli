package entities.objects;

/**
 * Created by Luca on 14/03/2017.
 */
public class CollectibleFactoryImpl implements CollectibleFactory {
    @Override
    public Collectible createPiece(int x, int y) {
        return new Piece(x, y);
    }

    @Override
    public Collectible createStar(int x, int y) {
        return new Star(x, y);
    }
}

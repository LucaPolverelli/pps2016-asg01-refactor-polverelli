package entities.objects;

/**
 * Created by Luca on 14/03/2017.
 */
public interface ObjectFactory {

    Object createBlock(int x, int y);
    Object createTunnel(int x, int y);

}

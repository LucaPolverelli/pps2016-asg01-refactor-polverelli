package entities.objects;

/**
 * Created by Luca on 14/03/2017.
 */
public class ObjectFactoryImpl implements ObjectFactory {
    @Override
    public Object createBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public Object createTunnel(int x, int y) {
        return new Tunnel(x, y);
    }
}

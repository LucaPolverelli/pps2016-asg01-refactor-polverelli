package entities.objects;

import java.awt.Image;

import entities.BasicEntity;

public class BasicObject extends BasicEntity implements Object{

    protected Image imageObject;

    public BasicObject(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public Image getImageObject() {
        return imageObject;
    }

}

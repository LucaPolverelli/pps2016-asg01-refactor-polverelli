package entities.objects;

import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Piece extends BasicObject implements Runnable, Collectible {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private int counter;

    public Piece(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imageObject = Utils.getImage(Res.IMG_PIECE1);
        Thread chronoPiece = new Thread(this, Res.IMGP_OBJECT_PIECE);
        chronoPiece.start();
    }

    @Override
    public Image getImageObject(){
        return imageOnMovement();
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

    private Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

}

package entities.objects;

/**
 * Created by Luca on 14/03/2017.
 */
public interface CollectibleFactory {

    Collectible createPiece(int x, int y);
    Collectible createStar(int x, int y);

}

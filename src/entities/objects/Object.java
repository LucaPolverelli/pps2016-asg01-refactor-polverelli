package entities.objects;

import entities.Entity;

import java.awt.*;

/**
 * Created by Luca on 12/03/2017.
 */
public interface Object extends Entity{

    Image getImageObject();

}

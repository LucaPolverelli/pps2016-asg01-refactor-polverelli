package entities.objects;

import utils.Res;
import utils.Utils;

/**
 * Created by Luca on 14/03/2017.
 */
public class Star extends BasicObject implements Collectible{

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Star(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imageObject = Utils.getImage(Res.IMG_STAR);
    }
}

package entities;

/**
 * Created by Luca on 12/03/2017.
 */
public interface Entity {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);

    void moveOnSceneShift();
}

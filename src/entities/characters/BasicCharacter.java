package entities.characters;

import java.awt.Image;

import entities.BasicEntity;
import entities.Entity;
import utils.Res;
import utils.Utils;

public class BasicCharacter extends BasicEntity implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_MARGIN = 5;
    protected boolean moving;
    protected boolean toRight;
    private int counter;
    protected boolean alive;

    private int firstEntityMarginLeft;
    private int firstEntityMarginRight;
    private int firstEntityMarginTop;
    private int firstEntityMarginBottom;

    private int secondEntityMarginLeft;
    private int secondEntityMarginLeftWithMargin;
    private int secondEntityMarginRight;
    private int secondEntityMarginRightWithMargin;
    private int secondEntityMarginTop;
    private int secondEntityMarginTopWithMargin;
    private int secondEntityMarginBottom;
    private int secondEntityMarginBottomWithMargin;


    public BasicCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    protected boolean hitAhead(Entity entity) {
        updateMargins(entity);

        if(entity instanceof BasicCharacter && !(this.isToRight())){
            return false;
        } else {
            return !(firstEntityMarginRight < secondEntityMarginLeft ||
                    firstEntityMarginRight > secondEntityMarginLeftWithMargin ||
                    firstEntityMarginTop <= secondEntityMarginBottom ||
                    firstEntityMarginBottom >= secondEntityMarginTop);
        }
    }

    protected boolean hitBack(Entity entity) {
        updateMargins(entity);

        return !(firstEntityMarginLeft > secondEntityMarginRight ||
                firstEntityMarginRight < secondEntityMarginRightWithMargin||
                firstEntityMarginTop <= secondEntityMarginBottom ||
                firstEntityMarginBottom >= secondEntityMarginTop);
    }

    protected boolean hitBelow(Entity entity) {
        updateMargins(entity);

        return !(firstEntityMarginRight < (entity instanceof BasicCharacter ? secondEntityMarginLeft : secondEntityMarginLeftWithMargin) ||
                firstEntityMarginLeft > (entity instanceof BasicCharacter ? secondEntityMarginRight : secondEntityMarginRightWithMargin) ||
                firstEntityMarginTop < secondEntityMarginBottom ||
                firstEntityMarginTop > (entity instanceof  BasicCharacter ? secondEntityMarginBottom : secondEntityMarginBottomWithMargin));
    }

    protected boolean hitAbove(Entity entity) {
        updateMargins(entity);

        return !(firstEntityMarginRight < secondEntityMarginLeftWithMargin ||
                firstEntityMarginLeft > secondEntityMarginRightWithMargin ||
                firstEntityMarginBottom < secondEntityMarginTop ||
                firstEntityMarginBottom > secondEntityMarginTopWithMargin);
    }

    public boolean isEntityNearby(Entity entity) {
        updateMargins(entity);

        return ((firstEntityMarginLeft > secondEntityMarginLeft - PROXIMITY_MARGIN &&
                        firstEntityMarginLeft < secondEntityMarginRight + PROXIMITY_MARGIN) ||
                (firstEntityMarginRight > secondEntityMarginLeft - PROXIMITY_MARGIN &&
                        firstEntityMarginRight < secondEntityMarginRight + PROXIMITY_MARGIN));
    }

    private void updateMargins(Entity entity){
        firstEntityMarginLeft = this.getX();
        firstEntityMarginRight = this.getX() + this.getWidth();
        firstEntityMarginTop = this.getY() + this.getHeight();
        firstEntityMarginBottom = this.getY();

        secondEntityMarginLeft = entity.getX();
        secondEntityMarginLeftWithMargin = entity.getX() + HIT_MARGIN;
        secondEntityMarginRight = entity.getX() + entity.getWidth();
        secondEntityMarginRightWithMargin = entity.getX() + entity.getWidth() - HIT_MARGIN;
        secondEntityMarginTop = entity.getY() + entity.getHeight();
        secondEntityMarginTopWithMargin = entity.getY() + entity.getHeight() + HIT_MARGIN;
        secondEntityMarginBottom = entity.getY();
        secondEntityMarginBottomWithMargin = entity.getY() + HIT_MARGIN;
    }

}

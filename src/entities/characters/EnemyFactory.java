package entities.characters;

/**
 * Created by Luca on 14/03/2017.
 */
public interface EnemyFactory {

    Enemy createMushroom(int x, int y);
    Enemy createTurtle(int x, int y);

}

package entities.characters;

import java.awt.Image;

import entities.objects.Collectible;
import entities.objects.Object;
import entities.objects.Star;
import game.Main;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int DEFAULT_JUMP = 42;

    private boolean jumping;
    private int jumpingExtent;
    private boolean power;
    private int jumpLimit;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.jumping = false;
        this.jumpingExtent = 0;
        this.jumpLimit = DEFAULT_JUMP;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < jumpLimit) {
            if (this.getY() > Main.scene.getHeightLimit()) {
                this.setY(this.getY() - 4);
            }else{
                this.jumpingExtent = jumpLimit;
            }
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    public void contactObject(Object object) {
        if (this.hitAhead(object) && this.isToRight() || this.hitBack(object) && !this.isToRight()) {
            Main.scene.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(object) && this.jumping) {
            Main.scene.setFloorOffsetY(object.getY());
        } else if (!this.hitBelow(object)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(object)) {
                Main.scene.setHeightLimit(object.getY() + object.getHeight());
            } else if (!this.hitAbove(object) && !this.jumping) {
                Main.scene.setHeightLimit(0);
            }
        }
    }

    public void contact(Character character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.isAlive() && !isPower()) {
                die();
            } else if(character.isAlive() && isPower()) {
                killCharacter(character);
            }
        } else if (this.hitBelow(character)) {
            killCharacter(character);
        }
    }

    public boolean contactCollectible(Collectible collectible) {
        if (this.hitBack(collectible) || this.hitAbove(collectible) || this.hitAhead(collectible)
                || this.hitBelow(collectible)) {
            if(collectible instanceof Star){
                startPower();
            }
            return true;
        }
        return false;
    }

    private void die(){
        this.setMoving(false);
        this.setAlive(false);
    }

    private void killCharacter(Character character){
        character.setMoving(false);
        character.setAlive(false);
    }

    private void startPower(){
        this.power = true;
        this.jumpLimit = DEFAULT_JUMP * 2;
        startPowerTimer();
    }

    private void stopPower(){
        this.power = false;
        this.jumpLimit = DEFAULT_JUMP;
    }

    private void startPowerTimer(){
        Runnable timer = new Runnable() {
            private static final int PAUSE = 10000;
            @Override
            public void run() {
                while(isPower()){
                    try {
                        Thread.sleep(PAUSE);
                    } catch (InterruptedException e) {
                    }
                    stopPower();
                }
            }
        };
        Thread chronometer = new Thread(timer);
        chronometer.start();
    }

    public boolean isPower(){
        return power;
    }

    public Image walk(String name){
        return super.walk(name, MARIO_FREQUENCY);
    }
}

package entities.characters;

import entities.Entity;

import java.awt.*;

/**
 * Created by Luca on 12/03/2017.
 */
public abstract class AbstractEnemy extends BasicCharacter implements Enemy {

    private int offsetX;

    public AbstractEnemy(int x, int y, int width, int height){
        super(x, y, width, height);
        offsetX = 1;
    }

    public void move() {
        this.setOffsetX(isToRight() ? 1 : -1);
        super.setX(super.getX() + this.getOffsetX());
    }

    @Override
    public void contact(Entity entity) {
        if (this.hitAhead(entity) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(entity) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage(){
        return this.getDeadImage();
    }

    public Image walk(){
        return this.doWalk();
    }

    public int deadOffsetY(){
        return this.getDeadOffsetY();
    }

    protected abstract Image getDeadImage();

    protected abstract Image doWalk();

    protected abstract int getDeadOffsetY();

    private void setOffsetX(int offsetX){
        this.offsetX = offsetX;
    }

    private int getOffsetX(){
        return this.offsetX;
    }

}

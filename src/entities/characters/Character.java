package entities.characters;

import entities.Entity;

import java.awt.Image;

public interface Character extends Entity{

	boolean isAlive();

	boolean isToRight();

	void setAlive(boolean alive);

	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	Image walk(String name, int frequency);

	boolean isEntityNearby(Entity entity);

}

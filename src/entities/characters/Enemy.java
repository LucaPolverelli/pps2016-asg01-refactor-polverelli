package entities.characters;

import entities.Entity;

import java.awt.*;

/**
 * Created by Luca on 12/03/2017.
 */
public interface Enemy extends Character {

    void move();

    void contact(Entity entity);

    Image deadImage();

    Image walk();

    int deadOffsetY();

}

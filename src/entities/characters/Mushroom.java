package entities.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Mushroom extends AbstractEnemy implements Runnable {

    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 15;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);

        Thread chronoMushroom = new Thread(this, Res.IMGP_CHARACTER_MUSHROOM);
        chronoMushroom.start();
    }

    @Override
    public void run() {
        while (this.alive) {
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

    protected Image getDeadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

    protected Image doWalk(){
        return super.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY);
    }

    protected int getDeadOffsetY(){
        return MUSHROOM_DEAD_OFFSET_Y;
    }

}

package entities.characters;

/**
 * Created by Luca on 14/03/2017.
 */
public class EnemyFactoryImpl implements EnemyFactory {
    @Override
    public Enemy createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public Enemy createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}

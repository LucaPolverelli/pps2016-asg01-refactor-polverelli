package entities.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Turtle extends AbstractEnemy implements Runnable {

    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int PAUSE = 15;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);

        Thread chronoTurtle = new Thread(this, Res.IMG_TURTLE_IDLE);
        chronoTurtle.start();
    }

    @Override
    public void run() {
        while (this.alive) {
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

    protected Image getDeadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

    protected Image doWalk(){
        return super.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY);
    }

    protected int getDeadOffsetY(){
        return TURTLE_DEAD_OFFSET_Y;
    }
}
